package com.pcb.commons.core.persistence;

import com.google.common.collect.Maps;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceUnitInfo;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JpaEntityManagerFactory {

    private static final Logger logger = LoggerFactory.getLogger(JpaEntityManagerFactory.class);

    private JpaEntityManagerFactory() {
    }

    public static EntityManagerFactory getEntityManagerFactory(@Nonnull Datastore datastore, @Nonnull Class<?>[] classes, @Nonnull Properties properties) {
        Objects.requireNonNull(datastore);
        Objects.requireNonNull(classes);
        Objects.requireNonNull(properties);

        logger.info("Bootstrapping EntityManagerFactory for datastore: {}", datastore.name());

        PersistenceUnitInfo persistenceUnitInfo = getPersistenceUnitInfo(datastore.name(), classes, properties);
        return new HibernatePersistenceProvider()
                .createContainerEntityManagerFactory(persistenceUnitInfo, Maps.newHashMap());
    }

    private static PersistenceUnitInfoImpl getPersistenceUnitInfo(@Nonnull String persistenceUnitName, @Nonnull Class<?>[] entityClasses, @Nonnull Properties properties) {
        Objects.requireNonNull(persistenceUnitName);
        Objects.requireNonNull(entityClasses);
        Objects.requireNonNull(properties);

        return new PersistenceUnitInfoImpl(persistenceUnitName, Stream.of(entityClasses).map(Class::getName).collect(Collectors.toList()), properties);
    }
}
