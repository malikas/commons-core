package com.pcb.commons.core.persistence;

import com.pcb.commons.util.config.ConfigLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;

public enum JpaPersistenceConnection {
    INSTANCE;

    private static final Logger logger = LoggerFactory.getLogger(JpaPersistenceConnection.class);
    private static final String HIBERNATE_PROPERTIES_FILE = "hibernate.properties";

    private final ConcurrentMap<Datastore, EntityManagerFactory> entityManagerFactoryByDatastore;
    private final AtomicBoolean shutdown;

    JpaPersistenceConnection() {
        this.entityManagerFactoryByDatastore = new ConcurrentHashMap<>();
        this.shutdown = new AtomicBoolean(false);
    }

    public static EntityManager getEntityManager(@Nonnull Datastore datastore, @Nonnull Class<?>[] classes) {
        EntityManagerFactory entityManagerFactory = INSTANCE.entityManagerFactoryByDatastore.get(datastore);
        if (entityManagerFactory == null) {
            synchronized (INSTANCE) {
                entityManagerFactory = INSTANCE.entityManagerFactoryByDatastore.get(datastore);
                if (entityManagerFactory == null) {
                    entityManagerFactory = JpaEntityManagerFactory.getEntityManagerFactory(datastore, classes, loadHibernateProperties());
                    INSTANCE.entityManagerFactoryByDatastore.put(datastore, entityManagerFactory);
                }
            }
        }

        return entityManagerFactory.createEntityManager();
    }

    private static Properties loadHibernateProperties() {
        return ConfigLoader.getPropertiesFromFile(HIBERNATE_PROPERTIES_FILE);
    }

    public void shutdown() {
        if (shutdown.getAndSet(true)) {
            logger.warn("Shutdown has been already requested. Skipping this attempt");
            return;
        }

        logger.info("Shutting down entityManagerFactoryByDatastore of size {}", entityManagerFactoryByDatastore.size());
        for (EntityManagerFactory entityManagerFactory : entityManagerFactoryByDatastore.values()) {
            entityManagerFactory.close();
        }
    }
}
