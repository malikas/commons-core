package com.pcb.commons.core.persistence;

import com.google.common.base.Preconditions;
import org.hibernate.jpa.HibernatePersistenceProvider;

import javax.annotation.Nonnull;
import javax.persistence.SharedCacheMode;
import javax.persistence.ValidationMode;
import javax.persistence.spi.ClassTransformer;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.PersistenceUnitTransactionType;
import javax.sql.DataSource;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class PersistenceUnitInfoImpl implements PersistenceUnitInfo {

    private static final String JPA_VERSION = "2.2";

    private final String persistenceUnitName;
    private final PersistenceUnitTransactionType transactionType;
    private final List<String> managedClassNames;
    private final List<String> mappingFileNames;
    private final Properties properties;
    private final List<ClassTransformer> transformers;

    private DataSource jtaDataSource;
    private DataSource nonjtaDataSource;

    public PersistenceUnitInfoImpl(@Nonnull String persistenceUnitName, @Nonnull List<String> managedClassNames, @Nonnull Properties properties) {
        this.persistenceUnitName = Preconditions.checkNotNull(persistenceUnitName, "persistenceUnitName cannot be null");
        this.managedClassNames = Preconditions.checkNotNull(managedClassNames, "managedClassNames cannot be null");
        this.properties = Preconditions.checkNotNull(properties, "properties cannot be null");

        this.transactionType = PersistenceUnitTransactionType.RESOURCE_LOCAL;
        this.mappingFileNames = new ArrayList<>();
        this.transformers = new ArrayList<>();
    }

    @Override
    public String getPersistenceUnitName() {
        return persistenceUnitName;
    }

    @Override
    public String getPersistenceProviderClassName() {
        return HibernatePersistenceProvider.class.getName();
    }

    @Override
    public PersistenceUnitTransactionType getTransactionType() {
        return transactionType;
    }

    @Override
    public DataSource getJtaDataSource() {
        return jtaDataSource;
    }

    @Override
    public DataSource getNonJtaDataSource() {
        return nonjtaDataSource;
    }

    @Override
    public List<String> getMappingFileNames() {
        return mappingFileNames;
    }

    @Override
    public List<URL> getJarFileUrls() {
        return Collections.emptyList();
    }

    @Override
    public URL getPersistenceUnitRootUrl() {
        return null;
    }

    @Override
    public List<String> getManagedClassNames() {
        return managedClassNames;
    }

    @Override
    public boolean excludeUnlistedClasses() {
        return false;
    }

    @Override
    public SharedCacheMode getSharedCacheMode() {
        return null;
    }

    @Override
    public ValidationMode getValidationMode() {
        return null;
    }

    @Override
    public Properties getProperties() {
        return properties;
    }

    @Override
    public String getPersistenceXMLSchemaVersion() {
        return JPA_VERSION;
    }

    @Override
    public ClassLoader getClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    @Override
    public void addTransformer(ClassTransformer classTransformer) {
        this.transformers.add(classTransformer);
    }

    @Override
    public ClassLoader getNewTempClassLoader() {
        return null;
    }
}
